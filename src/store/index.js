import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

function filtering(oldList, filtersSettings) {
  let newList = [...oldList];
  if (filtersSettings.number !== '') {
    newList = newList.filter((el) => el.number.includes(filtersSettings.number));
  }
  if (filtersSettings.fromDate !== '') {
    newList = newList.filter((el) => el.date >= filtersSettings.fromDate);
  }
  if (filtersSettings.byDate !== '') {
    newList = newList.filter((el) => el.date <= filtersSettings.byDate);
  }
  if (filtersSettings.type !== '') {
    newList = newList.filter((el) => el.type === filtersSettings.type);
  }
  return newList;
}

function sorting(oldList, mode) {
  const newList = [...oldList];
  switch (mode) {
    case 'number-ascending':
      return newList.sort((a, b) => (+a.number >= +b.number ? 1 : -1));
    case 'number-descending':
      return newList.sort((a, b) => (+a.number >= +b.number ? -1 : 1));
    case 'date-ascending':
      return newList.sort((a, b) => (a.date >= b.date ? 1 : -1));
    case 'date-descending':
      return newList.sort((a, b) => (a.date <= b.date ? 1 : -1));
    case 'type-ascending':
      return newList.sort((a, b) => (a.type >= b.type ? 1 : -1));
    case 'type-descending':
      return newList.sort((a, b) => (a.type >= b.type ? 1 : -1)).reverse();
    case 'letter-ascending':
      return newList.sort((a, b) => (+a.id >= +b.id ? 1 : -1));
    case 'letter-descending':
      return newList.sort((a, b) => (+a.id >= +b.id ? -1 : 1));
    default:
      return newList.sort((a, b) => (+a.id >= +b.id ? 1 : -1));
  }
}

export default new Vuex.Store({
  state: {
    cardsStorageData: [
      {
        id: 0,
        number: '8',
        date: '2021-06-13',
        type: 'RUED',
      },
      {
        id: 1,
        number: '11',
        date: '2020-04-24',
        type: 'RUEX',
      },
      {
        id: 2,
        number: '9',
        date: '2021-08-25',
        type: 'RUSG',
      },
      {
        id: 3,
        number: '13',
        date: '2020-11-06',
        type: 'RUEX',
      },
      {
        id: 4,
        number: '12',
        date: '2021-05-10',
        type: 'RUED',
      },
      {
        id: 5,
        number: '22',
        date: '2020-10-30',
        type: 'RUSG',
      },
      {
        id: 6,
        number: '25',
        date: '2021-09-02',
        type: 'RUEX',
      },
      {
        id: 7,
        number: '24',
        date: '2021-01-12',
        type: 'RUED',
      },
      {
        id: 8,
        number: '18',
        date: '2021-03-04',
        type: 'RUED',
      },
      {
        id: 9,
        number: '21',
        date: '2021-02-25',
        type: 'RUEX',
      },
    ],
    addFormShow: false,
    editFormShow: false,
    currentEditId: 0,
    filtersSettings: {
      number: '',
      fromDate: '',
      byDate: '',
      type: '',
    },
    sortingMode: '',
  },
  mutations: {
    INITIALIZE_STORE: (state) => {
      if (localStorage.getItem('store')) {
        Object.assign(state, JSON.parse(localStorage.getItem('store')));
      }
    },
    TOGGLE_ADD_FORM: (state) => {
      state.addFormShow = !state.addFormShow;
    },
    ADD_NEW_ITEM: (state, item) => {
      state.cardsStorageData.push(item);
    },
    DELETE_ITEM: (state, id) => {
      state.cardsStorageData = state.cardsStorageData.filter(
        (el) => el.id !== id,
      );
    },
    TOGGLE_EDIT_FORM: (state, id) => {
      state.editFormShow = !state.editFormShow;
      state.currentEditId = id;
    },
    EDIT_ITEM: (state, item) => {
      Vue.set(
        state.cardsStorageData,
        state.cardsStorageData.indexOf(
          state.cardsStorageData.find((el) => el.id === item.id),
        ),
        item,
      );
    },
    SET_FILTERS_SETTINGS: (state, object) => {
      state.filtersSettings = object;
    },
    SET_SORTING_MODE: (state, mode) => {
      state.sortingMode = mode;
    },
  },
  actions: {
    INITIALIZE_STORE({ commit }) {
      commit('INITIALIZE_STORE');
    },
    TOGGLE_ADD_FORM({ commit }) {
      commit('TOGGLE_ADD_FORM');
    },
    ADD_NEW_ITEM({ commit }, item) {
      commit('ADD_NEW_ITEM', item);
    },
    DELETE_ITEM({ commit }, id) {
      commit('DELETE_ITEM', id);
    },
    TOGGLE_EDIT_FORM({ commit }, id) {
      commit('TOGGLE_EDIT_FORM', id);
    },
    EDIT_ITEM({ commit }, item) {
      commit('EDIT_ITEM', item);
    },
    SET_FILTERS_SETTINGS({ commit }, object) {
      commit('SET_FILTERS_SETTINGS', object);
    },
    SET_SORTING_MODE({ commit }, mode) {
      commit('SET_SORTING_MODE', mode);
    },
  },
  getters: {
    LIST_STATE(state) {
      return state.cardsStorageData;
    },
    ADD_FORM_STATE(state) {
      return state.addFormShow;
    },
    EDIT_FORM_STATE(state) {
      return state.editFormShow;
    },
    CURRENT_EDIT_ID_STATE(state) {
      return state.currentEditId;
    },
    FILTERS_SETTINGS_STATE(state) {
      return state.filtersSettings;
    },
    SORTING_MODE_STATE(state) {
      return state.sortingMode;
    },
    FILTERED_SORTED_STATE(state) {
      return sorting(
        filtering(state.cardsStorageData, state.filtersSettings),
        state.sortingMode,
      );
    },
  },
  modules: {},
});
