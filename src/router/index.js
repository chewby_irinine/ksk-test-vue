import Vue from 'vue';
import VueRouter from 'vue-router';
import CardsView from '@/views/CardsView.vue';
import ATableView from '@/views/ATableView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/cards',
  },
  {
    path: '/cards',
    name: 'cards',
    component: CardsView,
  },
  {
    path: '/table',
    name: 'table',
    component: ATableView,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
