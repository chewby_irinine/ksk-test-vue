import Vue from 'vue';
import messageFunctionsMixin from '@/components/a-table/stubs/mixins/messageFunctionsMixin';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.mixin(messageFunctionsMixin);

Vue.config.productionTip = false;

new Vue({
  data: {
    messages: {
      'rowsCounter.of': 'из',
    },
  },
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});
